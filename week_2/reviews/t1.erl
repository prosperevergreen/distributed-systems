-module(t1).

-export([operation/0]).

operation() ->
    keep_alive(fun() -> register(operation,self()),
        receive
            {addition, Val1, Val2} -> io:format("~n~p~n",[Val1 + Val2]);
            {subtraction, Val1, Val2} -> io:format("~n~p~n",[Val1 - Val2]);
            {multiplication, Val1, Val2} -> io:format("\n~p\n",[Val1 * Val2]);
            {division, Val1, Val2} -> io:format("\n~p\n", [Val1 / Val2])
        end
    end).

keep_alive(Fun) ->
    Pid = spawn(Fun),
    on_exit(Pid,fun(_) -> keep_alive(Fun) end).

on_exit(Pid,Fun) ->
    spawn(fun() -> process_flag(trap_exit,true),
        link(Pid),
        receive
            {'EXIT',Pid,Why} -> Fun(Why)
        end
    end).