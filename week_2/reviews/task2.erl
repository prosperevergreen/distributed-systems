-module(task2).
-export([start/0, server/0, keep_alive/1, on_exit/2]).

%% Task 2.1, server that is actually similar to the divider example, it waits input 
%% To start, run commands at shell: c(task2). task2:start().
%% Then you can send computation to server like server ! {addition, 1,2} 
%% in format {oper, A, B} where oper can be atom called addition,
%% subtraction, multiplication and division, 
%% then it sends the value to the client, that need to be registered beforehand
server() ->
    keep_alive(
      fun() -> 
	      register(server,self()),
	      receive
		  ({addition, Val1,Val2}) ->
		      client ! Val1 + Val2;
		  ({substraction, Val1,Val2}) -> 
		      client ! Val1 - Val2;
		  ({multiplication, Val1,Val2}) -> 
		      client ! Val1 * Val2;
		  ({division, Val1,Val2}) -> 
		      client ! Val1 / Val2;
		  _ ->
		      io:format("~n No valid input ~n")
	      end
      end).

keep_alive(Fun) ->
    Pid = spawn(Fun),
    on_exit(Pid,fun(_) -> keep_alive(Fun) end).

on_exit(Pid,Fun) ->
    spawn(fun() -> 
		  process_flag(trap_exit,true),
		  link(Pid),
		  receive
		      {'EXIT',Pid,Why} -> Fun(Why)
		  end
	  end).

start() ->
    server(), %% Start the server process
    register(client, %% simple client so values can be seen (server doesnt print to input so this is needed
	     spawn(fun()-> 
			   receive 
			       Val -> io:format("VALUE: ~p~n", [Val]) 
			   end 
		   end)
	    ).
