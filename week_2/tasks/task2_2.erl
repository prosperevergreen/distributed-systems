-module(task2_2).
-export([on_exit/2, keep_alive/1, server/0, start_server/0, client/1, start_client/1]).

% To compile the code run: c(task2_2).
% To start the server run: task2_2:start_server().
% To test client and server run: task2_2:start_client([{ addition, 2, 3 }, { multiplication, 5, 6 }]).

% Function that handles the exit state of a process
on_exit(Pid,Fun) ->
  spawn(
    fun() -> 
      process_flag(trap_exit,true),
      link(Pid),
      receive
        {'EXIT',Pid,Why} -> Fun(Why)
      end
    end).

% Function that keeps the arg function process alive
keep_alive(Fun) ->
  Pid = spawn(Fun),
  on_exit(Pid,fun(_) -> keep_alive(Fun) end).

% Server function that handles operations
server() ->
  register(server, self()),
  receive
    {Oper, Client_PID} ->
      case Oper of
        { addition, Val1, Val2 } -> Client_PID ! (Val1 + Val2);
        { subtraction, Val1, Val2 } -> Client_PID ! (Val1 - Val2);
        { multiplication, Val1, Val2 } -> Client_PID ! (Val1 * Val2);
        { division, Val1, Val2 } -> Client_PID ! (Val1 / Val2)
      end
  end.

% Function to start the server
start_server() ->
  keep_alive(fun() -> server() end).

% Client function that sends request to server
client([]) -> 
  io:format("~n~p~n", ["Client: Finished"]);
client([HeadOper | TailOpers]) ->
  server ! { HeadOper, self() },
  receive
    Reply -> 
      io:format("~n~p~n", [Reply]),
      client(TailOpers)
  after 
    1000 ->
      client(TailOpers)
  end.

% Spawns new client process
start_client(Operations) ->
  spawn(task2_2, client, [Operations]).


