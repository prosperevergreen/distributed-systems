-module(task2_4).
-export([start/0, process/3]).

% To compile the code run: c(task2_4).
% To start, run: task2_4:start().
% To quit a process e.g. p0 run: p0 ! quit.

% A function the micics a process
process(Processes, Interested_tokens, Got_token) ->
  io:format("~p: interested-in: ~p, got: ~p~n", [self(), Interested_tokens, Got_token]),
  timer:sleep(1000),
  case lists:member(Got_token, Interested_tokens) of
    true ->
      io:format("~p: using token: ~p~n", [self(), Got_token]),
      timer:sleep(1000),
    false ->
      [Pid ! {request, Interested_tokens, this()} || Pid <- Processes],
  end,
  io:format("~p: sent token: ~p~n", [self(), Got_token]),
  receive
    quit -> io:format("~p quit!!!~n", [self()]);
    {token, Token} -> process(Next_process, Interested_tokens, Token);
    {request, Tokens} ->
      case lists:member(Got_token, Tokens, PID) of
        true -> process(Next_process, Interested_tokens, Tokens)
  end.

% Starts the (4) processes.
start() ->
  register(p0, spawn(task2_4, process, [[p1,p2,p3], [school_db, paint_db], school_db])),
  register(p1, spawn(task2_4, process, [[p0,p2,p3], [school_db, work_db], empty])),
  register(p2, spawn(task2_4, process, [[p0,p1,p3], [school_db, office_db], house_db])),
  register(p3, spawn(task2_4, process, [[p0,p1,p2], [house_db, school_db], empty])).
