-module(task2_3).
-export([start/0, process/3]).

% To compile the code run: c(task2_3).
% To start, run: task2_3:start().
% To quit a process e.g. p0 run: p0 ! quit.

% A function the micics a process
process(Next_process, Interested_tokens, Got_token) ->
  io:format("~p: interested-in: ~p, got: ~p~n", [self(), Interested_tokens, Got_token]),
  timer:sleep(1000),
  case lists:member(Got_token, Interested_tokens) of
    true ->
      io:format("~p: using token: ~p~n", [self(), Got_token]),
      timer:sleep(1000),
      Next_process ! {token, Got_token};
    false ->
      Next_process ! {token, Got_token}
  end,
  io:format("~p: sent token: ~p~n", [self(), Got_token]),
  receive
    quit -> 
      io:format("~p quit!!!~n", [self()])
    {token, Token} ->
        process(Next_process, Interested_tokens, Token)
  end.

% Starts the (4) processes.
start() ->
  register(p0, spawn(task2_3, process, [p1, [school_db, house_db], school_db])),
  register(p1, spawn(task2_3, process, [p2, [work_db, paint_db], work_db])),
  register(p2, spawn(task2_3, process, [p3, [school_db, paint_db], empty])),
  register(p3, spawn(task2_3, process, [p0, [house_db, school_db], empty])).
