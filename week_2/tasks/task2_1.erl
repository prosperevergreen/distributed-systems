-module(task2_1).
-export([on_exit/2, keep_alive/1, server/0, start/0]).

% To compile the code run: c(task2_1).
% To start the server run: task2_1:start().
% To test operations run: server ! {addition, 2, 3}.

% Function that handles the exit state of a process
on_exit(Pid,Fun) ->
  spawn(
    fun() -> 
      process_flag(trap_exit,true),
      link(Pid),
      receive
        {'EXIT',Pid,Why} -> Fun(Why)
      end
    end).

% Function that keeps the arg function process alive
keep_alive(Fun) ->
  Pid = spawn(Fun),
  on_exit(Pid,fun(_) -> keep_alive(Fun) end).

% Server function that handles operations
server() ->
  register(server, self()),
  receive
    { addition, Val1, Val2 } -> io:format("~n~p~n", [Val1 + Val2]);
    { subtraction, Val1, Val2 } -> io:format("~n~p~n", [Val1 - Val2]);
    { multiplication, Val1, Val2 } -> io:format("~n~p~n", [Val1 * Val2]);
    { division, Val1, Val2 } -> io:format("~n~p~n", [Val1 / Val2])
  end.

% Function to start the server
start() ->
  keep_alive(fun() -> server() end).
