% Task: Same as Task1.2 but now use register to register the first process and communicate by the registered name.

-module(task1_3).
-import(task1_1, [get_next_date/1]).
-export([start/1, start/0, client/2, server/0]).


client(0,_) ->
    server ! quit,
    io:format("client finished~n", []);
client(N, Date) ->
    io:format("client -> server [current date]: ~p~n", [Date]),
    server ! {Date, self()},
    receive
        NextDate ->
          io:format("client <- server [next date]: ~p~n", [NextDate]),
          client(N - 1, NextDate)
    end.


server() ->
  receive
    quit -> io:format("server Finished~n", []);
    {Date, Client_PID} -> 
      io:format("server <- client [current date]: ~p~n", [Date]),
      NextDate = get_next_date(Date),
      io:format("server -> client [next date]: ~p~n", [NextDate]),
      Client_PID ! NextDate,
      server()
  end.

% call start with any date start({Year, Month, Day}).
start(Date) ->
    register(server, spawn(task1_3, server, [])),
    spawn(task1_3, client, [3, Date]).

% call start with no argument i.e. start(). uses {2001,12,29}
start() -> start({2001,12,29}).