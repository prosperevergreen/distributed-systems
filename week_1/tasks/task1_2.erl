% Task: 
% Write a function that waits for a message that is either a process id with a date tuple (like in Task 1.1) in which case it will send back the next date, and if atom quit, it will quit.
% Write another function that sends three process id, data tuples to another process (that implements the function above), and then it sends quit. The dates returned are to be printed.
% Arrange the process communication the same way as in tut15 example.

-module(task1_2).
-import(task1_1, [get_next_date/1]).
-export([start/1, start/0, client/3, server/0]).


client(0,_, Server_PID) ->
    Server_PID ! quit,
    io:format("client finished~n", []);
client(N, Date, Server_PID) ->
    io:format("client -> server [current date]: ~p~n", [Date]),
    Server_PID ! {Date, self()},
    receive
        NextDate ->
          io:format("client <- server [next date]: ~p~n", [NextDate]),
          client(N - 1, NextDate, Server_PID)
    end.


server() ->
  receive
    quit -> io:format("server Finished~n", []);
    {Date, Client_PID} -> 
      io:format("server <- client [current date]: ~p~n", [Date]),
      NextDate = get_next_date(Date),
      io:format("server -> client [next date]: ~p~n", [NextDate]),
      Client_PID ! NextDate,
      server()
  end.

% call start with any date start({Year, Month, Day}).
start(Date) ->
    Server_PID = spawn(task1_2, server, []),
    spawn(task1_2, client, [3, Date, Server_PID]).

% call start with no argument i.e. start(). uses {2001,12,29}
start() -> start({2001,12,29}).