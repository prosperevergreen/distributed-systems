% Task: Same as Task1.2 but now use register to register the first process and communicate by the registered name.

-module(task1_4).
-import(task1_1, [get_next_date/1]).
-export([start/1, start/0, client/1, server/0]).


client([]) ->
    server1 ! quit,
    server2 ! quit,
    io:format("client finished~n", []);
client([Date | Tail]) ->
    io:format("client -> server [current date]: ~p~n", [Date]),
    server1 ! {Date, self()},
    server2 ! {Date, self()},
    receive
        NextDate ->
          io:format("client <- server [next date]: ~p~n", [NextDate]),
          client(Tail)
    end.


server() ->
  receive
    quit -> io:format("server Finished~n", []);
    {Date, Client_PID} -> 
      io:format("server <- client [current date]: ~p~n", [Date]),
      NextDate = get_next_date(Date),
      io:format("server -> client [next date]: ~p~n", [NextDate]),
      Client_PID ! NextDate,
      server()
  end.

% call start with any date start({Year, Month, Day}).
start(Dates) ->
    register(server1, spawn(task1_4, server, [])),
    register(server2, spawn(task1_4, server, [])),
    spawn(task1_4, client, [Dates]),
    spawn(task1_4, client, [Dates]).

% call start with no argument i.e. start(). uses {2001,12,29}
start() -> start([{2001,12,29}, {1999, 11, 13}]).