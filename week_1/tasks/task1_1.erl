% Task: 
%
% Write an Erlang function that, given a date as a tuple {Year,Month,Day} returns the next date. 
% There should be nothing particularly difficult in the algorithms, but you have to notice leap years  and the fact that year 0 does not exist.
% For leap year computation, see: https://en.wikipedia.org/wiki/Leap_year
% You can google for the leap year code but I encourage you to write it yourself.
% Please note that you need this function in the other exercise tasks this week.

-module(task1_1).
-export([get_next_date/1]).

% Helper functions
-define(is_year(Y), (is_integer(Y) andalso Y >= 1)).
-define(is_month(M), (is_integer(M) andalso M >= 1 andalso M =< 12)).
-define(can_be_day(D), (is_integer(D) andalso D >= 1 andalso D =< 31 )).
-define(is_leap(Y), (((Y rem 4 == 0) andalso (Y rem 100 /= 0)) or (Y rem 400 == 0))).
-define(max_31days(M), lists:member(M,[1, 3, 5, 7, 8, 10, 12])).
-define(max_30days(M), lists:member(M,[4, 6, 9, 11])).
-define(is_max_month(M), M == 12).

% Check that day is valid and/or max day of the month
is_day({Y, M, D}) ->
  Is_31days = ?max_31days(M),
  Is_30days = ?max_30days(M),
  if
    Is_31days andalso D =< 31 ->
      {true, D == 31};
    Is_30days andalso D =< 30 ->
      {true, D == 30};
    ?is_leap(Y) andalso D =< 29 -> 
      {true, D == 29};
    not ?is_leap(Y) andalso D =< 28 ->
      {true, D == 28};
    true -> {false, never}
  end.

% Throw error if wrong date is entered
invalid_date() -> throw("Error: Invalid Date. get_next_date({Year, Month, Day}) has valid values, Year: 1..., Month: 1...12 and Day: 1...28,29,30,31").

% Gets the next date from the current date
% Call get_next_date({Y, M, D}) e.g. get_next_date({2001, 11, 30}).
get_next_date({Y, M, D}) when ?is_year(Y), ?is_month(M), ?can_be_day(D) ->
  {Is_valid_day, Is_max_day} = is_day({Y, M, D}),
  case Is_valid_day of
    true -> 
      case Is_max_day of
        true ->
          case ?is_max_month(M) of
            false -> {Y, M+1, 1};
            true -> {Y+1, 1, 1}
          end;
        false -> {Y, M, D + 1}
      end;
    _ -> invalid_date()
  end;
get_next_date({_, _, _}) -> 
  invalid_date().

