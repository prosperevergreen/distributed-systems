-module(d1_1).
-export([next_date/1]).

next_date({Y, M, D}) ->
    Is_invalid = (Y < 1) or (D > num_days(M, Y)),
    Is_last_day = D == num_days(M, Y),
    if
        Is_invalid -> throw(invalid_date);
        M == dec, D == 31 -> {Y + 1, jan, 1};
        Is_last_day -> {Y, next_month(M), 1};
        true -> {Y, M, D + 1}
    end.

next_month(M) ->
    case M of
        jan -> feb;
        feb -> mar;
        mar -> apr;
        apr -> may;
        may -> jun;
        jun -> jul;
        jul -> aug;
        aug -> sep;
        sep -> oct;
        oct -> nov;
        nov -> dec;
        dec -> jan
    end.

num_days(M, Y) ->
    Is_leap = is_leap(Y),
    case M of
        jan -> 31;
        feb when Is_leap -> 29;
        feb -> 28;
        mar -> 31;
        apr -> 30;
        may -> 31;
        jun -> 30;
        jul -> 31;
        aug -> 31;
        sep -> 30;
        oct -> 31;
        nov -> 30;
        dec -> 31;
        _ -> throw(invalid_month)
    end.

is_leap(Y) ->
    if
        Y rem 400 == 0 -> true;
        Y rem 100 == 0 -> false;
        Y rem 4 == 0 -> true;
        true -> false
    end.