-module(ex1_4_process_next_date).
-import(ex1_1_next_date, [next_date/1]).

-export([start/0, get_next_date/0, client/2]).

client(Server1, Server2) ->
  receive
    {send, Dates} ->
      handle_send(Dates, Server1),
      handle_send(Dates, Server2),
      client(Server1, Server2);
    Data ->
      io:format("Client received data: ~p~n", [Data]),
      client(Server1, Server2)
  end.

handle_send(Dates, Server) ->
  send_request(self(), Server, Dates),
  io:format("Client ~p requesting all data~n", [self()]),
  Server ! quit.

send_request(_, _, []) -> ok;
send_request(Client, Server, [Date]) -> Server ! {Client, Date};
send_request(Client, Server, [Date | Rest]) ->
  send_request(Client, Server, [Date]),
  send_request(Client, Server, Rest).

get_next_date() ->
  receive
    {Pid, {_, _, _} = Date} ->
      NextDate = next_date(Date),

      [{registered_name, RegisteredName} | _] = erlang:process_info(self()),
      io:format("~p server returning data to process ~p~n", [RegisteredName, Pid]),

      Pid ! NextDate,
      get_next_date();
    quit ->
      ok
  end.

start() ->
  register(next_date_process1, spawn(ex1_4_process_next_date, get_next_date, [])),
  register(next_date_process2, spawn(ex1_4_process_next_date, get_next_date, [])),
  Client1 = spawn(ex1_4_process_next_date, client, [next_date_process1, next_date_process2]),
  Client2 = spawn(ex1_4_process_next_date, client, [next_date_process1, next_date_process2]),

  Dates = [{1990, 12, 24}, {2020, 12, 31} , {2020, 2, 29}],
  Client1 ! {send, Dates},
  Client2 ! {send, Dates}.