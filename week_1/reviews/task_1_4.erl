-module(task_1_4).
-import(h, [nextDate/1]).
-import(lists, [last/1, droplast/1]).
-export([start/0, sender/1, receiver/0]).

sender([]) ->
    receiver_1 ! quit,
    receiver_2 ! quit;
sender(dates) ->
    receiver ! {last(dates), self()},
    receive
        {Year, Month, Day} ->
            io:format("Next date is ~p/~p/~p~n", [Year, Month, Day])
    end,
    sender(droplast(dates)).

receiver() ->
    receive
        quit ->
            exit("quit received");
        {{Year, Month, Day}, PID} ->
            io:format("Getting next date for ~p/~p/~p~n", [Year, Month, Day]),
            PID ! nextDate({Year, Month, Day}),
            receiver()
    end.

start() ->
    register(receiver_1, spawn(task_1_4, receiver, [])),
    register(receiver_2, spawn(task_1_4, receiver, [])),
    spawn(task_1_4, sender, [[{1000, 12, 31}, {2020, 2, 28}, {2022, 6, 6}]]),
    spawn(task_1_4, sender, [[{1000, 12, 31}, {2020, 2, 28}, {2022, 6, 6}]]).