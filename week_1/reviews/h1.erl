-module(h1).
-export([nextDate/1]).


%TASK FUNCTION. Provide (INT, INT, INT) -> {1,2,3}
nextDate({YEAR, MONTH, DAY}) ->
    MonthDays = monthDates({YEAR,MONTH}),
    if 
        DAY + 1 > MonthDays -> incrementMonth(YEAR,MONTH);
        true -> {YEAR, MONTH, DAY + 1}
    end.

incrementMonth(YEAR,MONTH) -> 
    if 
        MONTH == 12 -> {incrementYear(YEAR), 1, 1};
        true -> {YEAR, MONTH + 1, 1}
    end.

incrementYear(YEAR) ->
    if 
        YEAR == -1 -> 1;
        true -> YEAR + 1
    end.

monthDates({YEAR,MONTH}) -> 
    Leap = leapYear(YEAR),
    case MONTH of
        1 -> 31;
        2 when Leap == leapYear -> 29;
        2 -> 28;
        3 -> 31;
        4 -> 30;
        5 -> 31;
        6 -> 30;
        7 -> 31;
        8 -> 31;
        9 -> 30;
        10 -> 31;
        11 -> 30;
        12 -> 31
    end. 

leapYear(YEAR) ->
    if 
        YEAR rem 400 == 0 -> leapYear;
        YEAR rem 100 == 0 -> notLeapYear;
        YEAR rem 4 == 0 -> leapYear;
        true -> notLeapYear
    end.