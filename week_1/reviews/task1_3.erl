-module(task1_3).
-export([start/0, date_inc/0, caller/1, next_date/1]).

date_inc() ->
    receive
        quit -> io:fwrite("Date increaser quit\n",[]);
        {{Y,M,D}, Caller_PID} -> 
            io:fwrite("Increaser was asked to increase date ~p-~p-~p\n",[Y,M,D]),
            Caller_PID ! next_date({Y,M,D}),
            date_inc()
    end.

caller([]) ->
    date_inc ! quit,
    io:fwrite("Caller quit\n");


caller([Head|Tail]) ->
    date_inc ! {Head, self()},
    receive
        {Y,M,D} -> io:fwrite( "Caller received date ~p-~p-~p\n", [Y,M,D])
    end,
    caller(Tail).

start() ->
    register(date_inc, spawn(task1_3, date_inc, [])),
    spawn(task1_3, caller, [ [ {2022,1,31}, {2000,2,29}, {-1,12,31}] ]).



% next_date functionality from Task1.1

next_date({Year,Month,Day}) -> 
    N_Day = Day rem get_day_num({Year,Month,Day}) + 1,
    N_Month = 
        if 
            N_Day < Day -> (Month rem 12) + 1;
            true        -> Month
        end,
    N_Year =
        if 
            (N_Month < Month) and (Year == -1)  -> 1;
            (N_Month < Month)                   -> Year + 1;
            true                                -> Year
        end,
    {N_Year,N_Month,N_Day}.

get_day_num({Year,Month,_}) ->
    case Month of
        1 -> 31;
        2 -> 
            if 
                Year rem 4 /= 0     -> 28;
                Year rem 100 /= 0   -> 29;
                Year rem 400 /= 0   -> 28;
                true                -> 29
            end;
        3 -> 31;
        4 -> 30;
        5 -> 31;
        6 -> 30;
        7 -> 31;
        8 -> 31;
        9 -> 30;
        10 -> 31;
        11 -> 30;
        12 -> 31
    end.