-module(task13).
-export([start/0, nextDate/1, pong/0]).
nextDate({Year, Month, Day}) ->
    if 
        Year > 0, Month > 0, Month < 13, Day > 0, Day < 32 ->
            Leap = if
                trunc(Year / 400) * 400 == Year ->
                    leap;
                trunc(Year / 100) * 100 == Year ->
                    not_leap;
                trunc(Year / 4) * 4 == Year ->
                    leap;
                true ->
                    not_leap
            end,

            Max_day = case Month of
                9 -> 30;
                4 -> 30;
                6 -> 30;
                11 -> 30;
                2 when Leap == leap -> 29;
                2 -> 28;
                1 -> 31;
                3 -> 31;
                5 -> 31;
                7 -> 31;
                8 -> 31;
                10 -> 31;
                12 -> 31
            end,

            if 
                Day == Max_day ->
                    if 
                        Month == 12 ->
                            pong ! {self(), {Year + 1, Month, 1}};
                        Month /= 12 ->
                            pong ! {self(), {Year, Month + 1, 1}}
                    end;
                Day /= Max_day ->
                    pong ! {self(), {Year, Month, Day + 1}}
            end;

        Year =< 0; Month =< 0; Month > 12; Day =< 0; Day > 31 ->
            pong ! quit
    end.
    
pong() ->
    receive
        quit ->
            io:format("The given date format is invalid~n", []);
        {_, {Year, Month, Day}} ->
            io:format("~p/~p/~p~n", [Year, Month, Day])
    end.

start()->
    register(pong, spawn(task13, pong, [])),
    spawn(task13, nextDate, [{2024,2,28}]).