-module(task_1_2).
-import(task_1_1, [nextDate/1]).
-export([start/0, nextDateProcess/0, sendDates/2]).
-define(Dates, [ { 1349, 12, 24 }, { 1990, 6, 6 }, { 2022, 1, 29 } ]).

nextDateProcess() ->
    receive
        { PID, quit } ->
            exit(self(), quit),
            PID ! quit;
        { PID, Date } ->
            PID ! nextDate(Date),
            nextDateProcess()
    end.

sendDates(0, PID) ->
    PID ! { self(), quit };

sendDates(N, PID) when N =< 3 ->
    D = lists:nth(N, ?Dates),
    PID ! { self(), D },
    receive
        quit ->
            exit(self(), quit);
        Date ->
            io:format("~w~n", [ Date ])
    end,
    sendDates(N-1, PID).

start() ->
    NextDatePID = spawn(task_1_2, nextDateProcess, []),
    spawn(task_1_2, sendDates, [3, NextDatePID]).