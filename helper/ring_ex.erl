% Should work, but bugs are possible, as always.
-module(ring_ex).
-define(P1_no, 3).
-define(P2_no, 4).
-define(P3_no, 1).
-define(P4_no, 56).
-define(P5_no, 43).
-define(P6_no, 2).
-compile(export_all).
-define(Timeout, 100).


start_ring(My_no) ->
  io:format("Starting a new ring. I am ~p~n", [self()]),
  ring(My_no, [self()]).

% join a ring by contacting Ring_member
join_ring(My_no, Ring_member) ->
     io:format("Joining a ring through ~p. I am ~p with number ~p~n", [Ring_member, self(),My_no]),
     Ring_member ! {join, self()},
     receive
       {new_ring, Ring_member, Ring} -> 
           io:format("Joined the ring. I am ~p, the new ring is ~p~n", [self(), Ring]),
	   ring(My_no, Ring)
       after ?Timeout -> io:format("Timed out joining the ring, I am ~p~n", [self()])
     end.

ring(My_no, Ring) ->
  receive
     {fail, Pid} ->
        io:format("Got fail from ~p. Failing. I am ~p~n", [Pid, self()]);
     {terminate, Pid} ->
        io:format("Got terminate from ~p. Finishing. I am ~p~n", [Pid, self()]);
     % upon receiving terminate_cmd, send terminate to everyone and finish.
     {terminate_cmd, Pid} ->
        io:format("Got terminate_cmd from ~p. Sending terminate to all and finish. I am ~p~n", [Pid, self()]),
	[P ! {terminate, self()} || P <- Ring, P /= self()];
     {join, Pid} ->
        io:format("~p wants to join the ring. I am ~p and the new ring is ~p~n", [Pid, self(), [Pid|Ring]]),
	[P ! {new_ring, self(), [Pid | Ring]} || P <- [Pid|Ring], P /= self()],
	ring(My_no, [Pid|Ring]);
     {new_ring, Pid, NewRing} ->
        io:format("Got the new ring from ~p. I am ~p, the new ring is ~p~n", [Pid, self(), NewRing]),
	ring(My_no, NewRing);
     {are_you_alive, Pid} ->
        io:format("Got are_you_alive from ~p. Answering alive. I am ~p~n", [Pid, self()]),
	Pid ! {alive, self()},
	ring(My_no, Ring);
     {recover_ring_cmd, Pid} ->
        io:format("Got recover_ring_cmd from ~p. I am ~p, the ring is ~p~n", [Pid, self(), Ring]),
	[ P ! { are_you_alive, self()} || P <- Ring, P /= self() ],
	get_alive_sites (My_no, Ring, [self()])	
 % timing out here could lead to asking if everyone is alive
 % and each process could have a different timeout period
 % then you could play by terminating some process and seeing the recovery of the ring
    end.

get_alive_sites (My_no, Ring, Responses) when length(Ring) == length (Responses) ->
   io:format("Everyone was found alive.~n"),
   ring (My_no, Ring); % false alarm, all are well
get_alive_sites (My_no, Ring, Responses) ->
   io:format("Collecting live sites, and have received: ~p~n",[Responses]),
   receive {alive, Pid} -> get_alive_sites (My_no, Ring, [Pid|Responses])
   after ?Timeout ->
      io:format("Timed out waiting, I am ~p~n", [self()]),
      [P ! {new_ring, self(), Responses} || P <- Responses, P /= self()],
      ring(My_no, Responses)
   end.
    

ring_successor(Me, [P|_], Last) when Me == Last -> P;
ring_successor(Me, [P1|[P2|_]], _) when P1 == Me -> P2;
ring_successor(Me, [_|Procs], Last) -> ring_successor(Me, Procs, Last).
    


control(P1,P2) ->
    timer:sleep(?Timeout),
    P2 ! {fail, self() },
    timer:sleep(?Timeout),    
    P1 ! {recover_ring_cmd, self()},
    timer:sleep(5*?Timeout),
    P1 ! {terminate_cmd, self()},
    timer:sleep(5*?Timeout).


start() ->
    P1 = spawn(ring_ex, start_ring, [?P1_no]),
    P2 = spawn(ring_ex, join_ring, [?P2_no, P1]),
    P3 = spawn(ring_ex, join_ring, [?P3_no, P1]),
    P4 = spawn(ring_ex, join_ring, [?P4_no, P1]),
    P5 = spawn(ring_ex, join_ring, [?P5_no, P1]),
    spawn(ring_ex, control, [P1,P2]),
    ok.