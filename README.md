# Distributed Systems
## COMP.CS.340
### Version: 2021-2022 (TAU); 2022-2023 (TAU); 2023-2024 (TAU)

# LEARNING OUTCOMES
After the course, the student can implement distributed systems using the high-level functional programming language Erlang. The student knows various correctness criteria for such systems, can implement them in a system, and knows the risks involved if the system does not fulfill these criteria.

# CONTENT

- Communicating processes in Erlang
- Distributed system software architecture in Erlang
- Synchronization
- Replication and consistency
- Fault tolerance
- Security


# Start the development environment

```bash
# Change directory to dev env
cd .devcontainer

# start docker container
docker-compose up -d

# use any of the terminals

# Using SH

# A
docker exec -it erlang sh 

# B
docker exec -it erlang /bin/sh

# Using BASH

# A
docker exec -it erlang bash

# B
docker exec -it erlang /bin/bash


# Change directory to the task folder
cd week1/tasks

# Start erlang shell
erl
>
> halt(). # Exit shell
>
> q(). # Alternative quit command
```